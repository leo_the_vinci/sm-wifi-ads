<?php
require_once 'Mobile_Detect.php';
$machine = new Mobile_Detect;

$deviceType = ($machine->isMobile() ? ($machine->isTablet() ? 'tablet' : 'phone') : 'computer');
$scriptVersion = $machine->getScriptVersion();
?>
<html>
<head>
<style>
  html{
    overflow: hidden;
  }

  body{
    margin:0px;
  }

  #ad {
    opacity: 0;
  }

  #ad img{
    width:100%;
    max-width:100%;
    height: 100%;
  }

  .landscape{
      height: 100%!important;
  }

  .portrait{
      width: auto!important;
      height: 100%!important;
      left: 0;
      right: 0;
      margin: 0 auto;
      position: absolute;
  }

</style>
<script type="text/javascript">
    /*var slowLoad = window.setTimeout( function() {
      var content = '<!-- Image Tag (Tag for Images in newsletters only) //TAG for network 1697: FourPointZero Live  // Website: Itoohl Dev // Page: Home // Placement: Starbucks ad (6245588)  // created at: Jun 28, 2016 3:38:08 AM  --><a href="http://adserver.adtech.de/adlink/3.0/1697/6245588/0/0/ADTECH;grp=[group];cookie=no;uid=no;" target="_blank"><img src="http://adserver.adtech.de/adserv/3.0/1697/6245588/0/0/ADTECH;grp=[group];cookie=no;uid=no" border="0" height="0" width="0" alt="[Alt-Text]"></a><!-- End of Image Tag -->';
        document.getElementById("ad").innerHTML = content;
    }, 10 );

    document.addEventListener( 'load', function() {
        window.clearTimeout( slowLoad );
    }, false );*/
</script>
<?php include_once("analytics.php"); ?>
</head>
<body>
  <div id="ad">
    <?php switch ($deviceType) {
      case 'phone':
      if($machine->isiOs()):?>
      <!-- Image Tag (Tag for Images only) //TAG for network 1697: FourPointZero Live  // Website: SM WiFi // Page: Test // Placement: Test-default-414 x 736 (6388978)  // created at: Jan 11, 2017 8:50:20 AM  -->
<a href="http://adserver.adtech.de/adlink/3.0/1697/6388978/0/6550/ADTECH;grp=[group]"  target="_blank"><img src="http://adserver.adtech.de/adserv/3.0/1697/6388978/0/6550/ADTECH;grp=[group]" border="0" height="736" width="414" alt="[Alt-Text]"></a>
<!-- End of Image Tag -->
      <?php else:?>
        <!-- Image Tag (Tag for Images only) //TAG for network 1697: FourPointZero Live  // Website: SM WiFi // Page: Test // Placement: Test-default-414 x 736 (6388978)  // created at: Jan 11, 2017 8:50:20 AM  -->
<a href="http://adserver.adtech.de/adlink/3.0/1697/6388978/0/6550/ADTECH;grp=[group]"  target="_blank"><img src="http://adserver.adtech.de/adserv/3.0/1697/6388978/0/6550/ADTECH;grp=[group]" border="0" height="736" width="414" alt="[Alt-Text]"></a>
<!-- End of Image Tag -->
      <?php endif;?>
  <?php break;
      case 'tablet':?>
      <!-- Image Tag (Tag for Images only) //TAG for network 1697: FourPointZero Live  // Website: SM WiFi // Page: Test // Placement: Test-default-768 x 1024 (6389686)  // created at: Jan 11, 2017 9:03:10 AM  -->
<a href="http://adserver.adtech.de/adlink/3.0/1697/6389686/0/3670/ADTECH;grp=[group]"  target="_blank"><img src="http://adserver.adtech.de/adserv/3.0/1697/6389686/0/3670/ADTECH;grp=[group]" border="0" height="1024" width="768" alt="[Alt-Text]"></a>
<!-- End of Image Tag -->
  <?php break;
      case 'computer':?>
       <!-- Image Tag (Tag for Images only) //TAG for network 1697: FourPointZero Live  // Website: SM WiFi // Page: Test // Placement: Test-default-960 x 640 (6377793)  // created at: Jan 10, 2017 5:48:31 AM -->
<a href="http://adserver.adtech.de/adlink/3.0/1697/6377793/0/4776/ADTECH;grp=[group]"  target="_blank"><img src="http://adserver.adtech.de/adserv/3.0/1697/6377793/0/4776/ADTECH;grp=[group]" border="0" height="640" width="960" alt="[Alt-Text]"></a>
<!-- End of Image Tag -->
  <?php break;
      default:?>
       <!-- Image Tag (Tag for Images only) //TAG for network 1697: FourPointZero Live  // Website: SM WiFi // Page: Test // Placement: Test-default-960 x 640 (6377793)  // created at: Jan 10, 2017 5:48:31 AM -->
<a href="http://adserver.adtech.de/adlink/3.0/1697/6377793/0/4776/ADTECH;grp=[group]"  target="_blank"><img src="http://adserver.adtech.de/adserv/3.0/1697/6377793/0/4776/ADTECH;grp=[group]" border="0" height="640" width="960" alt="[Alt-Text]"></a>
<!-- End of Image Tag -->
      <?php break;
    } ?>
  </div>
<script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script>
  <script>
    /*function redirectDelay(){
      window.top.location.href = "https://www.smsupermalls.com/index";
    }
    setTimeout(redirectDelay, 8000);*/
    $(window).load(function() {
    var real_height = $('#ad img').height()
    var real_width = $('#ad img').width()
    var aspect_ratio = real_width/real_height;

    if(aspect_ratio > 1){
      $('#ad img').addClass('landscape');
      parent.postMessage("landscape",'http://ec2-52-221-4-159.ap-southeast-1.compute.amazonaws.com/');
    }else if(aspect_ratio < 1){
      $('#ad img').addClass('portrait');
      parent.postMessage("portrait",'http://ec2-52-221-4-159.ap-southeast-1.compute.amazonaws.com/');
    }else{
      $('#ad img').addClass('landscape');
      parent.postMessage("landscape",'http://ec2-52-221-4-159.ap-southeast-1.compute.amazonaws.com/');
    }
      console.log(real_width);
      console.log(real_height);
      console.log(aspect_ratio);
      //setTimeout(function(){$('#ad').css('opacity','1');}, 500);
      $('#ad').css('opacity','1');
  });
  </script>
</body>
</html>
